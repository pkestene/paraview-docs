.. include:: ../macros.hrst
.. include:: ../abbreviations.hrst

.. _chapter:Animation:

Animation
#########

In |ParaView|, you can create an animated scene where input data but also
analysis and visualization parameters can change at each timestep.

When loading a time varying dataset, |ParaView| automatically loads the
timesteps information. Then you can choose the current time, and the pipeline
will be recomputed accordingly.

A property animation is created by recording a series of keyframes.
At each keyframe, you set values for the properties of the readers, sources, and
filters that make up the visualization pipeline, as well as the position and
orientation of the camera. Once you have chosen the parameters, you can play
through the animation.

Also, the results of the animation can be saved to different format from the
:guilabel:`File`: menu.

 - :guilabel:`Save Animation`: images files (one image per animation frame) or movies (depends on available codecs)
 - :guilabel:`Save Geometry`: all geometries in scene with |ParaView|'s PVD file format
 - :guilabel:`Save Extracts`: save datasets and screenshots as configured in the pipeline with :guilabel:`Extractors` sources.
 - :guilabel:`Export Animated Scene`: for external viewer, as vtk.js format

image files  or to a movie file. The geometry
rendered at each frame can also be saved in  file format, which
can be loaded back into |ParaView| as a time varying dataset.


Time Manager
==============

``Time Manager``  is the main place where to control time.
This is accessible from the :guilabel:`View` menu.

.. figure:: ../images/TimeManagerAnimations.png
    :name: fig:TimeManager
    :width: 80%
    :align: center

    Time Manager.

This panel is presented as a table.
Above the table are controls that administer how time progresses in the animation.

Within the table, the tracks are organized into two sections.
The first displays information about time varying data present in the pipeline.
Each row match one temporal pipeline source, and its associated timesteps.
The first row in the table, simply labeled ``Time``, shows the scene times, *i.e.*
the times that will be used when playing the scene. That can be a combination of pipeline source data times.
The current displayed time is indicated both in the Time field at the top
and with a thick, vertical line within the table.

Note that both the first and the last time labels of the timeline have special background.
Click on them to enter  ``Start Time``   and  ``End Time``  entry-boxes and configure the start and end times
for the animation. By default, when you load time varying datasets, the start
and end times are automatically adjusted to cover the entire time range present
in the data. The lock check-buttons just next to the  ``Start Time``   and  ``End Time``
labels will prevent this from happening, so that you can ensure that your
animation covers a particular time domain of your choosing.

In the second part come the animation tracks.
The ``Animations`` row contains widget to creates animations.

.. figure:: ../images/AnimationTracks.png
    :name: fig:AnimationTracks
    :width: 80%
    :align: center

    Animation Tracks.

You choose a data source and then a particular property of the data
source in the bottom row. To create an animation track with keyframes for that
property, click the  :guilabel:`+`   on the right-hand side; this will create a new track.
In the figure, tracks already exist for *SphereSource1*'s
``Phi Resolution``   property and for the camera's position. To delete a track, press the
:guilabel:`X`   button. You can temporarily disable a track by unchecking the check box
on the left of the track. To enter values for the property, double-click within
the white area to the right of the track name. This will bring up the
``Animation Keyframes``   dialog. Double-clicking in the camera entry brings up a
dialog like the one in :numref:`fig:EditingCameraTrack`.

.. figure:: ../images/AnimationCameraInterpolationEditor.png
    :name: fig:EditingCameraTrack
    :width: 80%
    :align: center

    Editing the camera track.

From the  ``Animation Keyframes``   dialog, you can press :guilabel:`New` to create new keyframes.
You can also press  :guilabel:`Delete`   or  :guilabel:`Delete All`   to delete some or all of the keyframes. Clicking  :guilabel:`New`   will
add a new row to the table. In any row, you can click within the  ``Time``   column to
choose a particular time for the keyframe, and you can click in the right-hand column to
enter values for the parameter. The exact user interface components that let you
set values for the property at the keyframe time vary. When available, you can
change the interpolation between two keyframes by double-clicking on the central
interpolation column.

Within the tracks of the  ``Time Manager``  , the place in time where each keyframe
occurs is shown as a vertical line. The values chosen for the property at that
time and the interpolation function used between that value and the next are
shown as text, when appropriate. In the previous figure, for example, the sphere
resolution begins at 10 and then changes to 20, varying by linear interpolation
between them. The camera values are too lengthy to show as text so they are not
displayed in the track, but we can easily see that there are four keyframes
spaced throughout the animation. The vertical lines in the tracks themselves may
be dragged, so you can easily adjust the time at which each keyframe occurs.


.. admonition:: **Did you know?**
  :class: tip

   You can quickly add a simple animation track and edit the keyframe without using the
   ``Time Manager``   by using  ``Animation Shortcut``  .
   First, enable  ``Show Animation Shortcut``    from
   ``Settings``   dialog (on the  ``General``   tab, search for the option by name or switch to advanced view).
   Then, several of the animatable properties on the  ``Properties``   panel will have a |pqVcrPlay| icon.
   Click this icon to add a new animation track for this property and edit it.

.. figure:: ../images/AnimationShortcut.png
    :width: 40%
    :align: center

Scene time configuration
=========================

Time controls
^^^^^^^^^^^^^
The  ``Time Manager``   has a header bar that lets you control some properties of the
animation itself, as you can see in :numref:`fig:TimeManagerWidgets`.

.. figure:: ../images/TimeManagerWidgets.png
    :name: fig:TimeManagerWidgets
    :width: 80%
    :align: center

    Time Manager Widgets.

The  ``Time``   entry-box shows the current animation time, which is the same as shown by a
vertical marker in this view. You can change the current animation time by
either entering a value in this box, if available, or by dragging the vertical
marker.
The second entry-box is the timestep index. You can also edit it.

If the scene does not use any pipeline sources to get its list of times (see below), you can also
edit ``Number of frames`` to set the total number of frames for the scene.

The ``Stride`` parameter, available in advanced mode, allows to jump by this number of timesteps
when navigating through times.

Time list
^^^^^^^^^

|ParaView| supports two modes to configure scene times.
The time list can either come from the data themself or it can be generated.
We call them ``Snap to Timestep`` and ``Sequence`` respectively.

.. figure:: ../images/TimeManagerSequence.png
    :name: fig:TimeManagerSequence
    :width: 80%
    :align: center

    Time Manager Sequence.


In ``Sequence`` mode, the number of frames is controlled by the  ``No. Frames``
spinbox. Times are evenly spaced between Start and End time.
This is the preferred mode when working with non-temporal data.
This is activated when none of the ``Time`` tracks are enabled, i.e.
the checkbox left to the ``Time`` label of first track is disabled.

In the figure, the main timeline has 12 ticks. They do not match
the times from the data, as expected for a sequence.

.. figure:: ../images/TimeManagerSnap.png
    :name: fig:TimeManagerSnap
    :width: 80%
    :align: center

    Time Manager Snap to timesteps.


In ``Snap To TimeSteps`` mode, the number of frames in the animation is
determined by the number of time values in the dataset being animated and thus cannot be changed.
This is the animation mode used by default by |ParaView| when a dataset with time values is loaded.
Then playing the animation means playing through the time values of the dataset one after the other.
This is enabled when at least one ``Time`` track is checked.

In the figure, only ``can.ex2`` is checked so the scene knows only about its timesteps:
the main timeline has same ticks as the ``can.ex2`` track.

.. admonition:: **Did you know?**
  :class: tip

   You can change the precision (number of significant digits) displayed by the animation clock
   by changing the ``Animation Time Precision`` value in the ``Settings`` under the ``General`` tab.


.. figure:: ../images/AnimationTimePrecision.png
    :width: 40%
    :align: center

Animating time-varying data
===========================

We saw that the main scene of |ParaView| can know about the times available
from a pipeline source, such as a reader that produces time varying data.
Then a current time is set at the scene level, and by default the
pipeline is updated to the relevant time.

But what happen when asking for a time that does not exist in the data?
How to do some properties animation at a fixed data time?
Can I animate the time itself?

|ParaView| offers one answer to all those questions: the ``Time Keeper``.
This is an animation track, visible only in *advanced* mode.
The ``Time Keeper`` maps the scene time to a new time used to update the pipeline.
The default mapping simply maps the scene time to itself.

But you can uncouple the data time from the animation time so that you can create
complex animations.

If you double-click in the  ``TimeKeeper – Time``   track, the  ``Animation Keyframes``
dialog, an example of which is shown in :numref:`fig:ControllingDataTime`, appears. In this dialog,
you can make data time progress in three fundamentally different ways. If the
``Animation Time``   radio-button is selected, the data time will be tied to and
scaled with the animation time so that, as the animation progresses, you will see
the data evolve naturally.  If you want to ignore the time varying nature of the data,
you can select  ``Constant Time``   instead. In this case, you choose a
particular time value at which the data will be displayed for the duration of
the animation. Finally, you can select the  ``Variable Time``   radio-button to have
full control over data time and to control it as you do any other animatable
property in the visualization pipeline. In the example shown in
:numref:`fig:ControllingDataTime`, time is made to progress forward for the
first 15 frames of the animation, backward for the next 30, and forward
for the final 15.

.. figure:: ../images/ParaViewUsersGuideAnimationKeyframesTime.png
    :name: fig:ControllingDataTime
    :width: 80%
    :align: center

    Controlling Data Time with keyframes.

.. %Animation Settings
   ===================
   %
   %\begin{figure}[htb]
   %\begin{center}
   %\includegraphics[width=0.6\linewidth]{Images/ParaViewUsersGuideAnimationSettings.png}
   %\caption{Animation Settings}
   %.. _fig:AnimationSettings:
   %\end{center}
   %\end{figure}
   %
   %
   %Additional animation properties can be changed using the  ``Animation``   page in the
   %:guilabel:`Edit > Settings` dialog. Using these settings seen in
   %:numref:`fig:AnimationSettings`, you can
   %control whether geometry must be cached to improve playback performance during
   %looping, as well as the maximum size of geometry to cache to avoid memory
   %overflows.

Animating the camera
====================

Just like you can change parameters on sources and filters in an animation, you
can also change the camera parameters. You can add
animation tracks to animate the camera for all the 3D render views in the setup
separately. To add a camera animation track for a view, with the view selected,
choose ``Camera`` from the first drop-down menu. The second drop-down list allows
you to choose how to animate the camera. Then click on the  ``+``   button.
There are three possible options, each of which provides different mechanisms to specify
the keyframes. It's not possible to change the mode after the animation track
has been added, but you can simply delete the track and create a new one.

Follow data
^^^^^^^^^^^
In this mode, the camera focal point and position is changed to keep the data centered in the view.
It does not change the camera zoom level, however.

Interpolate cameras
^^^^^^^^^^^^^^^^^^^

In this mode, you specify camera position, focal point, view angle, and
up direction at each keyframe. The animation player interpolates between these
specified locations. As with other parameters, to edit the keyframes,
double-click on the track. It is also possible to capture the current location
as a keyframe by using the  ``Use Current``   button.

.. figure:: ../images/AnimationCameraInterpolationEditor.png
    :name: fig:SettingAnimationParameters
    :width: 80%
    :align: center

    Setting camera animation parameters.

It can be quite challenging to add enough keyframes correctly to ensure
that the animation results in a smooth visualization using this mode.
However, it is the preferred mode if you want fine control on a few cameras.

Follow path
^^^^^^^^^^^

In this mode, you get the opportunity to specify the path taken by the camera
position and the camera focal point. By default, the path is set up to orbit around
the selected objects. You can then edit the keyframe to change the paths.
This is the preferred mode if you want to control the global motion more than exact
points of view.

:numref:`fig:CreatingCameraPath` shows the dialog for editing these paths for
a keyframe. When Camera Position or Camera Focus is selected, a widget is shown
in the 3D view that can be used to set the path. Use |CTRL| + :kbd:`Left Click` to
insert new control points, and |SHIFT| + :kbd:`Left Click` to remove control
points. You can also toggle when the path should be closed or not.

.. figure:: ../images/AnimationCameraPathEditor.png
    :name: fig:CreatingCameraPath
    :width: 80%
    :align: center

    Creating a camera path.

.. |pqVcrPlay| image:: ../images/icons/pqVcrPlay.svg
   :width: 20px


This mode makes it possible to quickly create a camera animation in which the
camera revolves around objects of interest. Clicking on the ``Orbit`` button
will pop up a dialog where you can edit the orbit parameters such as the
center of revolution, the normal for the plane of revolution, and the origin (i.e., a
point on the plane where the revolution begins). By default, the ``Center`` is the
center of the bounds of the selected objects, the ``Normal`` is the current up direction
used by the camera, while the origin is the current camera position.

.. figure:: ../images/ParaViewUsersGuideCameraOrbit.png
    :name: fig:CreatingCameraOrbit
    :width: 80%
    :align: center

    Creating a camera orbit.


Playing an animation
====================

Once you have designed your animation, you can play through it with the VCR
controls toolbar seen in :numref:`fig:VCRControlsToolbar`.

.. figure:: ../images/VCRAndTimeControls.png
    :name: fig:VCRAndTimeControls
    :width: 80%
    :align: center

    ``VCR Controls``   and  ``Current Time Controls``   toolbars in |paraview|.

Note that the frames are rendered as fast as possible. Thus, the viewing
frame rate depends on the time needed to generate and render each frame.

When you play the animation, you can cache the geometric
output of the visualization pipeline in memory. When you replay the
animation, playback will be much faster because very little computation must be
done to generate the images.

Explore an animation using Python
=================================

The following Python commands allow you to get and explore an animation scene.

Just like you can navigate through timesteps in |paraview|'s user interface, you can use Python commands to do the same.
These commands are available through an animation scene object, retrieved with:

.. code-block:: python

   >>> scene = GetAnimationScene()

This object has several methods available to change which animation timestep is shown:

.. code-block:: python

   >>> scene.GoToFirst()
   >>> scene.GoToLast()
   >>> scene.GoToNext()
   >>> scene.GoToPrevious()

Additional methods are available to start and stop playback of the animation:

.. code-block

   >>> scene.Play()
   >>> scene.Stop()
   >>> scene.Reverse()

`Play()` plays the animation from current timestep to the last. `Reverse()` plays the animation in reverse from the current timestep to the first.

Finally, you can access the available times in all loaded datasets using

.. code-block

   >>> times = scene.TimeKeeper.TimestepValues
   >>> times
   [0.0, 0.1, 0.2]

The times are returned in a Python list.
