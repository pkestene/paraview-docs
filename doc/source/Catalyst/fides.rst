Fides Reader
============

An alternative to :ref:`Mesh Blueprints <ParaViewCatalystBlueprint>` is using the  `Fides`_ reader.
Fides uses a different data model based around JSON and ADIOS2. More details about the `Fides Data Model`_ page.
An example of using ParaViewCatalyst with Fides is located `here`__.

__ ParaViewCatalystFidesExample_

.. _`Fides`: https://fides.readthedocs.io/en/latest
.. _`Fides Data Model`: https://fides.readthedocs.io/en/latest/schema/schema.html
.. _`ParaViewCatalystFidesExample`: https://fides.readthedocs.io/en/latest/paraview/paraview.html#in-situ-visualization-with-paraview-catalyst-and-fides
