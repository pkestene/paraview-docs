Examples for Simulation Developers
==================================

These examples reside in the ParaView repository and are part of ParaView's CI process.
The core differences are in the Adaptor class of each example which is where the simulation data are converted to a conduit node and passed to ParaView for processing. For a detailed example regarding the structure of a Catalyst Adaptor see `here <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxFullExample/CatalystAdaptor.h>`__

To experiment with any example click on the download directory button in the GitLab interface and follow the instructions of :ref:`ParaViewCatalystGettingStarted` section.


* C driver that creates unstructured mesh in a single channel `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CFullExample>`__
* C++ driver that creates unstructured mesh in a single channel `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxFullExample>`__
* Fortran90 driver that creates uniform mesh in a single channel `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/Fortran90FullExample>`__
* Python driver that creates a uniform mesh in a single channel `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/PythonFullExample>`__

* C++ driver that creates uniform mesh and passes arguments to the catalyst script `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxImageDataExample>`__
* C++ driver that creates 2 multiblock unstructured meshes each one on its dedicated channel  `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxMultiChannelInputExample>`__
* C++ driver that uses the :ref:`multimesh protocol <ParaViewCatalystBlueprintMultiMeshProtocol>` `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxMultimesh>`__
* C++ driver that utilizes a polygonal mesh `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxPolygonal>`__
* C++ driver that utilizes a polyhedral mesh `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxPolyhedra>`__
* C++ dirver that utilizes an overlapping AMR dataset `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxOverlappingAMRExample>`__
* C++ driver that utilizes steering mode, where one can modify simulation parameters at runtime `[code] <https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Catalyst2/CxxSteeringExample>`__
